package dev.grgames.controllers;

import dev.grgames.GrGamesApplication;
import dev.grgames.process.Process;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LaunchController {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    @Autowired
    private Process process;
    @GetMapping("process/launch")
    public ResponseEntity launch(){
        logger.info("launch request received");
        try {
            process.execute();
            return new ResponseEntity(HttpStatus.OK);
        }catch (Exception e){
            logger.warn(e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
