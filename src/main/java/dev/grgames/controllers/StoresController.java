package dev.grgames.controllers;

import dev.grgames.controllers.model.StoreRequestResponse;
import dev.grgames.controllers.model.StoresResponse;
import dev.grgames.service.StoresService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/stores")
@AllArgsConstructor
public class StoresController {
    private static final Logger logger = LoggerFactory.getLogger(GenresController.class);

    private final StoresService storesService;

    @GetMapping("/{slug}")
    public ResponseEntity<StoreRequestResponse> getStore(@PathVariable String slug){
        logger.info("get store request received");
        Optional<StoreRequestResponse> storeResponse = storesService.getStore(slug);
        if(storeResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(storeResponse.get(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<StoresResponse> getStores(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer pageSize){
        logger.info("get stores request received");
        Optional<StoresResponse> storesResponse = storesService.getStores(PageRequest.of(page,pageSize));
        if(storesResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(storesResponse.get(), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<StoreRequestResponse> updateEntireStore(@RequestBody StoreRequestResponse store){
        logger.info("modify store request received");
        Optional<StoreRequestResponse> modifiedStoreResponse = storesService.modifyEntireStore(store);
        if(modifiedStoreResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(modifiedStoreResponse.get(), HttpStatus.OK);
    }

    @PatchMapping("/{slug}")
    public ResponseEntity<StoreRequestResponse> updatePartialStore(@PathVariable String slug, @RequestBody StoreRequestResponse incompleteStore) {
        logger.info("modify store request received");
        Optional<StoreRequestResponse> storeModified = storesService.modifyPartialStore(slug,incompleteStore);
        if(storeModified.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(storeModified.get(), HttpStatus.OK);
    }
}
