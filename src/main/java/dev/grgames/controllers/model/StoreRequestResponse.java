package dev.grgames.controllers.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class StoreRequestResponse implements Serializable {
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty(value = "domain")
    private String webSite;
    @JsonProperty
    private String image_background;
    @JsonProperty
    private String games_count;
}
