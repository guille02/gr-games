package dev.grgames.controllers.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;
@AllArgsConstructor
@Getter
public class GamesResponse implements Serializable {

    @JsonProperty
    private List<GameRequestResponse> results;
}
