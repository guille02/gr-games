package dev.grgames.controllers.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.grgames.data.models.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class GameRequestResponse implements Serializable {

    @JsonProperty
    private String slug;

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private Date released;

    @JsonProperty
    private String background_image;

    @JsonProperty
    private String background_image_additional;

    @JsonProperty
    private int playtime;

    @JsonProperty
    private String website;

    @JsonProperty(value = "rating_average")
    private Double ratingAverage;

    @JsonProperty
    private List<Genre> genres;

    @JsonProperty
    private List<Store> stores;

    @JsonProperty
    private List<Platform> platforms;

    @JsonProperty
    private List<Tag> tags;

    @JsonProperty
    private List<Developer> developers;

}
