package dev.grgames.controllers.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TagRequestResponse {
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty
    private String language;
    @JsonProperty
    private String games_count;
    @JsonProperty
    private String image_background;
}
