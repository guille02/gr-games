package dev.grgames.controllers.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class JwtExceptionResponse implements Serializable {
    private String message;

    private String url;

    private String method;

    private Integer status;

    private LocalDateTime timestamp;
}
