package dev.grgames.controllers.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.grgames.data.models.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class GenreRequestResponse implements Serializable, GameField {

    @JsonProperty
    private String slug;

    @JsonProperty
    private String name;

    @JsonProperty
    private String games_count;

    @JsonProperty
    private String image_background;

    @JsonProperty
    private String description;

}
