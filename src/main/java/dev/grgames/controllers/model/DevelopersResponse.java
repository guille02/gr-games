package dev.grgames.controllers.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
public class DevelopersResponse implements Serializable{

    @JsonProperty
    private List<DeveloperRequestResponse> results;
}
