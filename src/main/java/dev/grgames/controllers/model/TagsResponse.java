package dev.grgames.controllers.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class TagsResponse {

    @JsonProperty
    private List<TagRequestResponse> results;
}
