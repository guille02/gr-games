package dev.grgames.controllers;

import dev.grgames.controllers.model.DeveloperRequestResponse;
import dev.grgames.controllers.model.DevelopersResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import dev.grgames.service.DevelopersService;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/developers")
public class DevelopersController {

    @Autowired
    private DevelopersService developersService;

    private static final Logger logger = LoggerFactory.getLogger(DevelopersController.class);

    @GetMapping("/{slug}")
    public ResponseEntity<DeveloperRequestResponse> getDeveloper(@PathVariable String slug){
        logger.info("get developer request received");
        Optional<DeveloperRequestResponse> opDeveloper = developersService.getDeveloper(slug);
        if (opDeveloper.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(opDeveloper.get(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<DevelopersResponse> getDevelopers(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer pageSize){
        logger.info("get developers request received");
        Optional<DevelopersResponse> opDevelopers = developersService.getDevelopers(PageRequest.of(page,pageSize));
        if(opDevelopers.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(opDevelopers.get(), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<DeveloperRequestResponse> updateEntireDeveloper(@RequestBody DeveloperRequestResponse developerRequestResponse){
        logger.info("modify developer request received");
        Optional<DeveloperRequestResponse> opDeveloper = developersService.modifyEntireDeveloper(developerRequestResponse);
        if(opDeveloper.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(opDeveloper.get(), HttpStatus.OK);
    }

    @PatchMapping("/{slug}")
    public ResponseEntity<DeveloperRequestResponse> updatePartialDeveloper(@PathVariable String slug, @RequestBody DeveloperRequestResponse incompleteDeveloper){
        logger.info("modify developer request received");
        Optional<DeveloperRequestResponse> opDeveloper = developersService.modifyPartialDeveloper(slug,incompleteDeveloper);
        if(opDeveloper.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(opDeveloper.get(), HttpStatus.OK);
    }


}
