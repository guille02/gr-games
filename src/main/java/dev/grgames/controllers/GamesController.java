package dev.grgames.controllers;

import dev.grgames.controllers.model.GameRequestResponse;
import dev.grgames.controllers.model.GamesResponse;
import dev.grgames.data.models.GameField;
import dev.grgames.service.GamesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/games")
public class GamesController {
    private static final Logger logger = LoggerFactory.getLogger(GamesController.class);

    @Autowired
    private GamesService gamesService;

    @GetMapping("/{slug}")
    public ResponseEntity<GameRequestResponse> getGame(@PathVariable String slug) {
        logger.info("get game request received");
        Optional<GameRequestResponse> gameResponse = gamesService.getGame(slug);
        if(gameResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(gameResponse.get(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<GamesResponse> getGames(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genres,
            @RequestParam(required = false) String platforms,
            @RequestParam(required = false) String stores,
            @RequestParam(required = false) String tags,
            @RequestParam(required = false) String developers,
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer pageSize){
        logger.info("get games request received");
        Optional<GamesResponse> games = gamesService.getGames(PageRequest.of(page,pageSize),name,genres,platforms,stores,tags,developers,null);
        if(games.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(games.get(), HttpStatus.OK);
    }

    @GetMapping("/users/{username}")
    public ResponseEntity<GamesResponse> getGamesByUser(
            @PathVariable String username,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genres,
            @RequestParam(required = false) String platforms,
            @RequestParam(required = false) String stores,
            @RequestParam(required = false) String tags,
            @RequestParam(required = false) String developers,
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer pageSize){
        logger.info("get games by user request received");
        Optional<GamesResponse> games = gamesService.getGames(PageRequest.of(page,pageSize),name,genres,platforms,stores,tags,developers,username);
        if(games.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(games.get().getResults().isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(games.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{slug}")
    public ResponseEntity<?> deleteGame(@PathVariable String slug) {
        logger.info("delete game request received");
        gamesService.deleteGame(slug);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping()
    public ResponseEntity<GameRequestResponse> updateEntireGame(@RequestBody GameRequestResponse game) {
        logger.info("update game request received");
        Optional<GameRequestResponse> gameResponse = gamesService.modifyEntireGame(game);
        if (gameResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(gameResponse.get(), HttpStatus.CREATED);
    }

    @PatchMapping("/{slug}")
    public ResponseEntity<GameRequestResponse> updatePartialGame(@PathVariable String slug, @RequestBody GameRequestResponse incompleteGame) {
        logger.info("update game request received");
        Optional<GameRequestResponse> gameModified = gamesService.modifyPartialGame(slug,incompleteGame);
        if(gameModified.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(gameModified.get(),HttpStatus.OK);
    }

}
