package dev.grgames.controllers;

import dev.grgames.controllers.model.GenreRequestResponse;
import dev.grgames.controllers.model.GenresResponse;
import dev.grgames.service.GenresService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/genres")
@AllArgsConstructor
public class GenresController {

    private static final Logger logger = LoggerFactory.getLogger(GenresController.class);

    private final GenresService genresService;

    @GetMapping("/{slug}")
    public ResponseEntity<GenreRequestResponse> getGenre(@PathVariable String slug){
        logger.info("get genre request received");
        Optional<GenreRequestResponse> genreResponse = genresService.getGenre(slug);
        if(genreResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(genreResponse.get(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<GenresResponse> getGenres(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer pageSize){
        logger.info("get genres request received");
        Optional<GenresResponse> genres = genresService.getGenres(PageRequest.of(page,pageSize));
        if(genres.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(genres.get(), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<GenreRequestResponse> updateEntireGenre(@RequestBody GenreRequestResponse genre){
        logger.info("update genre request received");
        Optional<GenreRequestResponse> genreResponse = genresService.modifyEntireGenre(genre);
        if(genreResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(genreResponse.get(), HttpStatus.OK);
    }

    @PatchMapping("/{slug}")
    public ResponseEntity<GenreRequestResponse> updatePartialGenre(@PathVariable String slug, @RequestBody GenreRequestResponse incompleteGenre) {
        logger.info("update partial genre request received");
        Optional<GenreRequestResponse> genreResponse = genresService.modifyPartialGenre(slug,incompleteGenre);
        if(genreResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(genreResponse.get(), HttpStatus.OK);

    }
}
