package dev.grgames.controllers;


import dev.grgames.controllers.model.PlatformRequestResponse;
import dev.grgames.controllers.model.PlatformsResponse;
import dev.grgames.service.PlatformsService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/platforms")
@AllArgsConstructor
public class PlatformsController {
    private static final Logger logger = LoggerFactory.getLogger(PlatformsController.class);

    private final PlatformsService platformsService;

    @GetMapping("/{slug}")
    public ResponseEntity<PlatformRequestResponse> getPlatform(@PathVariable String slug){
        logger.info("get platform request received");
        Optional<PlatformRequestResponse> platformResponse = platformsService.getPlatform(slug);
        if(platformResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(platformResponse.get(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<PlatformsResponse> getPlatforms(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer pageSize){
        logger.info("get platforms request received");
        Optional<PlatformsResponse> platforms = platformsService.getPlatforms(PageRequest.of(page,pageSize));
        if(platforms.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(platforms.get(), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<PlatformRequestResponse> updateEntirePlatform(@RequestBody PlatformRequestResponse platformRequestResponse){
        logger.info("modify platform request received");
        Optional<PlatformRequestResponse> opPlatform = platformsService.modifyEntirePlatform(platformRequestResponse);
        if(opPlatform.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(opPlatform.get(), HttpStatus.OK);
    }

    @PatchMapping("/{slug}")
    public ResponseEntity<PlatformRequestResponse> updatePartialPlatform(@PathVariable String slug, @RequestBody PlatformRequestResponse incompletePlatform) {
        logger.info("modify platform request received");
        Optional<PlatformRequestResponse> platformModified = platformsService.modifyPartialPlatform(slug, incompletePlatform);
        if(platformModified.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(platformModified.get(), HttpStatus.OK);

    }
}
