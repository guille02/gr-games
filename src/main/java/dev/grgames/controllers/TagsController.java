package dev.grgames.controllers;

import dev.grgames.GrGamesApplication;
import dev.grgames.controllers.model.TagRequestResponse;
import dev.grgames.controllers.model.TagsResponse;
import dev.grgames.service.TagsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/tags")
public class TagsController {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);

    @Autowired
    private TagsService tagsService;

    @GetMapping("/{slug}")
    public ResponseEntity<TagRequestResponse> getTag(@PathVariable String slug){
        logger.info("get genre request received");
        Optional<TagRequestResponse> tagResponse = tagsService.getTag(slug);
        if(tagResponse.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(tagResponse.get(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<TagsResponse> getTags(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer pageSize) {
        logger.info("get genres request received");
        Optional<TagsResponse> tags = tagsService.getTags(PageRequest.of(page, pageSize));
        if (tags.isEmpty()) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<>(tags.get(), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<TagRequestResponse> updateEntireTag(@RequestBody TagRequestResponse tag){
        logger.info("modify genre request received");
        Optional<TagRequestResponse> modifiedTag = tagsService.modifyEntireTag(tag);
        if(modifiedTag.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(modifiedTag.get(),HttpStatus.OK);
    }

    @PatchMapping("/{slug}")
    public ResponseEntity<TagRequestResponse> updatePartialTag(@PathVariable String slug, @RequestBody TagRequestResponse incompleteTag){
        logger.info("modify genre request received");
        Optional<TagRequestResponse> modifiedTag = tagsService.modifyPartialTag(slug,incompleteTag);
        if(modifiedTag.isEmpty()) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(modifiedTag.get(),HttpStatus.OK);
    }


}
