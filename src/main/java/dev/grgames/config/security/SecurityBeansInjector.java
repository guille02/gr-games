package dev.grgames.config.security;

import dev.grgames.authentication.BasicAuthProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class SecurityBeansInjector {
    @Bean
    public BasicAuthProvider authenticationProvider(){return new BasicAuthProvider();}
}
