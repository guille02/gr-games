package dev.grgames.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.grgames.authentication.JwtService;
import dev.grgames.controllers.model.JwtExceptionResponse;
import dev.grgames.data.models.User;
import dev.grgames.service.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    private final UserService userService;

    private final ObjectMapper objectMapper;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        Pattern pattern = Pattern.compile("/api/v1/games/users/.*");
        Matcher matcher = pattern.matcher(request.getRequestURI());
        if (!matcher.matches()) {
            filterChain.doFilter(request, response);
            return;
        }

        String authHeader = request.getHeader("Authorization");
        if (!StringUtils.hasText(authHeader) || !authHeader.startsWith("Bearer ")) {return;}
        String accessToken = authHeader.split(" ")[1];

        try {
            String username = jwtService.getSubjectFromAccessToken(accessToken);
            Optional<User> user = userService.findByUsername(username);
            if(user.isPresent()){
                UserDetails userDetails = user.get();
                SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username, null, userDetails.getAuthorities()));
            }
            filterChain.doFilter(request, response);
        }catch (ExpiredJwtException | SignatureException | MalformedJwtException exception) {
            handleJwtException(exception, request, response);
        }
    }

    private void handleJwtException(Exception exception, HttpServletRequest request, HttpServletResponse response) throws IOException {
        JwtExceptionResponse jwtExceptionResponse = new JwtExceptionResponse();
        jwtExceptionResponse.setMessage(exception.getLocalizedMessage());
        jwtExceptionResponse.setUrl(request.getRequestURL().toString());
        jwtExceptionResponse.setMethod(request.getMethod());
        jwtExceptionResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        jwtExceptionResponse.setTimestamp(LocalDateTime.now());

        setResponseProperties(response, jwtExceptionResponse);
    }

    private void setResponseProperties(HttpServletResponse response, JwtExceptionResponse jwtExceptionResponse) throws IOException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write(objectMapper.writeValueAsString(jwtExceptionResponse));
    }
}
