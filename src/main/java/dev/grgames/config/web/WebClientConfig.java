package dev.grgames.config.web;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig  {

    @Bean
    public WebClient webClient(WebClient.Builder webClientBuilder){
        return  webClientBuilder
                .build();
    }
    @LoadBalanced
    @Bean
    public WebClient webClientInternal(WebClient.Builder webClientBuilder){
        return  webClientBuilder
                .build();
    }

    public static WebClient webClient(){
        return WebClient.builder().exchangeStrategies(ExchangeStrategies.builder()
                        .codecs(clientCodecConfigurer -> clientCodecConfigurer
                                .defaultCodecs()
                                .maxInMemorySize(16 * 1024 * 1024))
                        .build())
                .build();}
}
