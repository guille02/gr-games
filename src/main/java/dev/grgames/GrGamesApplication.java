package dev.grgames;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrGamesApplication {
	private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(GrGamesApplication.class, args);
		logger.info("---------------------------");
		logger.info("---       GR-GAMES      ---");
		logger.info("---------------------------");
	}

}
