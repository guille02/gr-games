package dev.grgames.process;

import dev.grgames.GrGamesApplication;
import dev.grgames.data.models.Game;
import dev.grgames.data.models.Genre;
import dev.grgames.data.models.Screenshot;
import dev.grgames.data.repositories.*;
import dev.grgames.data.request.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class Process {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    @Autowired
    GenresRequest genresRequest;
    @Autowired
    GamesRequest gamesRequest;
    @Autowired StoresRequest storesRequest;
    @Autowired
    PlatformsRequest platformsRequest;
    @Autowired
    ScreenshotsRequest screenshotsRequest;
    @Autowired TagsRequest tagsRequest;
    @Autowired
    DevelopersRequest developersRequest;
    @Autowired GenreRepository genreRepository;
    @Autowired
    GameRepository gameRepository;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    PlatformRepository platformRepository;
    @Autowired
    ScreenshotRepository screenshotRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    DeveloperRepository developerRepository;

    public void execute(){
        dbCleanup();
        refreshData();
    }

    private void refreshData(){
        List<Genre> genres = genresRequest.getData();
        genreRepository.saveAll(genresRequest.getData());
        storeRepository.saveAll(storesRequest.getData());
        platformRepository.saveAll(platformsRequest.getData());
        tagRepository.saveAll(tagsRequest.getData());
        developerRepository.saveAll(developersRequest.getData());
        List<Game> games = gamesRequest.getData();
        List<Screenshot> screenshots = getAndSetScreenshots(games);
        gameRepository.saveAll(games);
        screenshotRepository.saveAll(screenshots);
        logger.info("data reloaded successfully");
    }
    @Transactional
    private void dbCleanup(){
        try{
            screenshotRepository.deleteAll();
            gameRepository.deleteAll();
            genreRepository.deleteAll();
            storeRepository.deleteAll();
            platformRepository.deleteAll();
            tagRepository.deleteAll();
            developerRepository.deleteAll();
        }catch (DataAccessException e){
            logger.warn("error during database cleanup",e.getMessage());
        }
    }

    private List<Screenshot> getAndSetScreenshots(List<Game> games) {
        return games.stream()
                .flatMap(game -> screenshotsRequest.getData(game.getSlug()).stream()
                        .peek(screenshot -> screenshot.setGame(game)))
                .collect(Collectors.toList());
    }
}
