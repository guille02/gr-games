package dev.grgames.utils;

import dev.grgames.data.models.*;
import jakarta.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class SearchCriteria<Game> implements Specification<Game> {

    private final List<String> genres;
    private final List<String> platforms;
    private final List<String> stores;
    private final List<String> tags;
    private final List<String> developers;
    private final String username;

    public SearchCriteria(List<Genre> genres, List<Platform> platforms, List<Store> stores, List<Tag> tags ,List<Developer> developers,String username) {
        this.genres = getSlugs(genres);
        this.platforms = getSlugs(platforms);
        this.stores = getSlugs(stores);
        this.tags = getSlugs(tags);
        this.developers = getSlugs(developers);
        this.username = username;
    }


    @Override
    public Predicate toPredicate(Root<Game> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Expression<Boolean>> expressions = new ArrayList<>();
        List<Predicate> predicates = new ArrayList<>();

        if(genres != null && !genres.isEmpty()) {
            Join<Game, Genre> genresJoin = root.join("genres");
            expressions.add(criteriaBuilder.equal(criteriaBuilder.countDistinct(genresJoin), genres.size()));
            predicates.add(genresJoin.get("slug").in(genres));
        }

        if (platforms != null && !platforms.isEmpty()) {
            Join<Game, Platform> platformsJoin = root.join("platforms");
            expressions.add(criteriaBuilder.equal(criteriaBuilder.countDistinct(platformsJoin), platforms.size()));
            predicates.add(platformsJoin.get("slug").in(platforms));
        }

        if (stores != null && !stores.isEmpty()) {
            Join<Game, Store> storesJoin = root.join("stores");
            expressions.add(criteriaBuilder.equal(criteriaBuilder.countDistinct(storesJoin), stores.size()));
            predicates.add(storesJoin.get("slug").in(stores));
        }

        if (tags != null && !tags.isEmpty()) {
            Join<Game, Tag> tagsJoin = root.join("tags");
            expressions.add(criteriaBuilder.equal(criteriaBuilder.countDistinct(tagsJoin), tags.size()));
            predicates.add(tagsJoin.get("slug").in(tags));
        }

        if (developers != null && !developers.isEmpty()) {
            Join<Game, Developer> developersJoin = root.join("developers");
            expressions.add(criteriaBuilder.equal(criteriaBuilder.countDistinct(developersJoin), developers.size()));
            predicates.add(developersJoin.get("slug").in(developers));
        }

        if(StringUtils.hasLength(username)) {
            Join<Game, User> usersJoin = root.join("users");
            predicates.add(usersJoin.get("userName").in(List.of(username)));
        }

        query.groupBy(root.get("slug"));

        query.having(criteriaBuilder.and(expressions.toArray(new Predicate[0])));
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

    private List<String> getSlugs(List<? extends GameField> list){
        if(list == null) return null;
        return list.stream().map(GameField::getSlug).collect(Collectors.toList());
    }
}
