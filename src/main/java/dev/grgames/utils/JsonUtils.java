package dev.grgames.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.grgames.data.models.messaging.NewUserMessage;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class JsonUtils {
    private JsonUtils() {}

    public static Optional<NewUserMessage> fromJson(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return Optional.of(objectMapper.readValue(json, NewUserMessage.class));
        } catch (JsonProcessingException e) {
            LoggerFactory.getLogger(JsonUtils.class).warn("Error parse to json {}",e.getMessage());
        }
        return Optional.empty();
    }

}
