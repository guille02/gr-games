package dev.grgames.utils;

import dev.grgames.data.models.Game;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

@Component
public class Patcher {
    public void internPatcher(Object existingIntern, Object incompleteExtern) throws IllegalAccessException {
        Class<?> internClass= existingIntern.getClass();
        Field[] internFields=internClass.getDeclaredFields();
        for(Field field : internFields){
            field.setAccessible(true);
            Object value=field.get(incompleteExtern);
            if(value!=null){
                field.set(existingIntern,value);
            }
            field.setAccessible(false);
        }
    }
}
