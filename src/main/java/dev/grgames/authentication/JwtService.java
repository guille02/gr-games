package dev.grgames.authentication;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;

@Service
public class JwtService {

    @Value("${security.jwt.secret}")
    private String secret;

    public String getSubjectFromAccessToken(String jwt) {
        return extractAllClaims(jwt).getSubject();
    }

    private Claims extractAllClaims(String jwt) {
        return Jwts.parserBuilder().setSigningKey( generateKey() ).build()
                .parseClaimsJws(jwt).getBody();
    }

    private Key generateKey() {
        return Keys.hmacShaKeyFor(secret.getBytes());
    }
}
