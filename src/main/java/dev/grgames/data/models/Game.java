package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Game {
    @Id
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty
    @Column(columnDefinition = "LONGTEXT")
    private String description;
    @JsonProperty
    private Date released;
    @JsonProperty
    private String background_image;
    @JsonProperty
    private String background_image_additional;
    @JsonProperty
    private int playtime;
    @JsonProperty
    private String website;

    private Double ratingAverage;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "game-genre", joinColumns = @JoinColumn(name = "game_slug", referencedColumnName = "slug"),
            inverseJoinColumns = @JoinColumn(name = "genre_slug", referencedColumnName = "slug")
    )
    private List<Genre> genres;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "game-store", joinColumns = @JoinColumn(name = "game_slug", referencedColumnName = "slug"),
            inverseJoinColumns = @JoinColumn(name = "store_slug", referencedColumnName = "slug")
    )
    private List<Store> stores;

    @JsonProperty(value = "stores")
    @Transient
    private List<StoreElem> storeElems;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "game-platform", joinColumns = @JoinColumn(name = "game_slug", referencedColumnName = "slug"),
            inverseJoinColumns = @JoinColumn(name = "platform_slug", referencedColumnName = "slug")
    )
    private List<Platform> platforms;

    @JsonProperty(value = "platforms")
    @Transient
    private List<PlatformElem> platformElems;

    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "game-tag", joinColumns = @JoinColumn(name = "game_slug", referencedColumnName = "slug"),
            inverseJoinColumns = @JoinColumn(name = "tag_slug", referencedColumnName = "slug")
    )
    private List<Tag> tags;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "game-developer", joinColumns = @JoinColumn(name = "game_slug", referencedColumnName = "slug"),
            inverseJoinColumns = @JoinColumn(name = "developer_slug", referencedColumnName = "slug")
    )
    private List<Developer> developers;

    @ManyToMany(mappedBy = "userGames", cascade = CascadeType.REMOVE)
    private List<User> users;

    public Game (String slug, String name, String description, Date released, String background_image, String background_image_additional, int playtime, String website, Double ratingAverage, List<Genre> genres, List<Store> stores, List<Platform> platforms, List<Tag> tags, List<Developer> developers) {
        this.slug = slug;
        this.name = name;
        this.description = description;
        this.released = released;
        this.background_image = background_image;
        this.background_image_additional = background_image_additional;
        this.playtime = playtime;
        this.website = website;
        this.ratingAverage = ratingAverage;
        this.genres = genres;
        this.stores = stores;
        this.platforms = platforms;
        this.tags = tags;
        this.developers = developers;
    }

}
