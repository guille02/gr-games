package dev.grgames.data.models;

public interface GameField {
    String getSlug();
    String getName();

}
