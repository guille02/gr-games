package dev.grgames.data.models.security;

import dev.grgames.data.models.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccessToken {

    private String token;

    private Date expirationDate;

    private User user;
}
