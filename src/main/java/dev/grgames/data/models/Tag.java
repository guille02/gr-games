package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Tag implements GameField, Serializable {
    @Id
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty
    private String language;
    @JsonProperty
    private String games_count;
    @JsonProperty
    private String image_background;
    @JsonBackReference
    @ManyToMany(mappedBy = "tags", cascade = CascadeType.REMOVE)
    private List<Game> games;
    @ManyToMany(mappedBy = "userTags", cascade = CascadeType.REMOVE)
    private List<User> users;

    public Tag(String slug) {
        this.slug = slug;
    }

    public Tag(String slug, String name, String language, String games_count, String image_background) {
        this.slug = slug;
        this.name = name;
        this.language = language;
        this.games_count = games_count;
        this.image_background = image_background;
    }

}
