package dev.grgames.data.models.messaging;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewUserMessage implements Serializable {
    private String username;
    private String accessToken;
    private String refreshToken;
    private String role;
}
