package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Genre implements GameField, Serializable {

    @JsonProperty
    @Id
    private String slug;

    @JsonProperty
    private String name;

    @JsonProperty
    private String games_count;

    @JsonProperty
    private String image_background;

    @JsonProperty
    private String description;

    @JsonBackReference
    @ManyToMany(mappedBy = "genres", cascade = CascadeType.REMOVE)
    private List<Game> games;

    @ManyToMany(mappedBy = "userGenres", cascade = CascadeType.REMOVE)
    private List<User> users;

    public Genre(String slug) {
        this.slug = slug;
    }

    public Genre (String slug, String name, String games_count, String image_background, String description){
        this.slug = slug;
        this.name = name;
        this.games_count = games_count;
        this.image_background = image_background;
        this.description = description;
    }

}
