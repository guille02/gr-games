package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Platform implements GameField, Serializable {
    @Id
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty
    private String image_background;
    @JsonProperty
    private String games_count;
    @JsonBackReference
    @ManyToMany(mappedBy = "platforms", cascade = CascadeType.REMOVE)
    private List<Game> games;
    @ManyToMany(mappedBy = "userPlatforms", cascade = CascadeType.REMOVE)
    private List<User> users;

    public Platform(String slug) {
        this.slug = slug;
    }

    public Platform(String slug, String name, String image_background, String games_count) {
        this.slug = slug;
        this.name = name;
        this.image_background = image_background;
        this.games_count = games_count;
    }

}
