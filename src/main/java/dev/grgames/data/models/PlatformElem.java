package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlatformElem {
    @JsonProperty
    private Platform platform;

    public Platform getPlatform() {
        return platform;
    }
}
