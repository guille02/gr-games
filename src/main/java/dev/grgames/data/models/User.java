package dev.grgames.data.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@NoArgsConstructor
@Entity
public class User implements UserDetails, Serializable {
    @Id
    private String userName;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "rol_id")
    private Role role;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "user-game", joinColumns = @JoinColumn(name = "user_name", referencedColumnName = "userName"),
            inverseJoinColumns = @JoinColumn(name = "game_slug", referencedColumnName = "slug")
    )
    private List<Game> userGames;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "user-tag", joinColumns = @JoinColumn(name = "user_name", referencedColumnName = "userName"),
            inverseJoinColumns = @JoinColumn(name = "tag_slug", referencedColumnName = "slug")
    )
    private List<Tag> userTags;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "user-genre", joinColumns = @JoinColumn(name = "user_name", referencedColumnName = "userName"),
            inverseJoinColumns = @JoinColumn(name = "genre_slug", referencedColumnName = "slug")
    )
    private List<Genre> userGenres;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "user-platform", joinColumns = @JoinColumn(name = "user_name", referencedColumnName = "userName"),
            inverseJoinColumns = @JoinColumn(name = "platform_slug", referencedColumnName = "slug")
    )
    private List<Platform> userPlatforms;

    public User (String userName,Role role){
        this.userName = userName;
        this.role = role;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Stream.of(role)
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
