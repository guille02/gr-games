package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


public class StoreElem implements Serializable {
    @JsonProperty
    private Integer id;

    @JsonProperty()
    private Store store;

    public Store getStore() {
        return store;
    }

}
