package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Developer implements GameField, Serializable {
    @Id
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty
    private String games_count;
    @JsonProperty
    private String image_background;
    @JsonBackReference
    @ManyToMany(mappedBy = "developers", cascade = CascadeType.REMOVE)
    private List<Game> games;

    public Developer(String slug) {
        this.slug = slug;
    }

    public Developer(String slug, String name, String games_count, String image_background) {
        this.slug = slug;
        this.name = name;
        this.games_count = games_count;
        this.image_background = image_background;
    }

}
