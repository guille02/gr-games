package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

import java.io.Serializable;

@Entity
public class Screenshot implements Serializable {
    @Id
    @JsonProperty
    private Integer id;
    @JsonProperty
    private String image;
    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "game_slug", referencedColumnName = "slug")
    private Game game;

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

}
