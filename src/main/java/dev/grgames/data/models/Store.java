package dev.grgames.data.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Store implements GameField, Serializable {
    @Id
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty(value = "domain")
    private String website;
    @JsonProperty
    private String image_background;
    @JsonProperty
    private String games_count;
    @JsonBackReference
    @ManyToMany(mappedBy = "stores", cascade = CascadeType.REMOVE)
    private List<Game> games;

    public Store(String slug) {
        this.slug = slug;
    }

    public Store (String slug, String name, String website, String image_background, String games_count) {
        this.slug = slug;
        this.name = name;
        this.website = website;
        this.image_background = image_background;
        this.games_count = games_count;
    }
}
