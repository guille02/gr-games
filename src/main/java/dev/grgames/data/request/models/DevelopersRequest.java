package dev.grgames.data.request.models;

import dev.grgames.GrGamesApplication;
import dev.grgames.config.web.WebClientConfig;
import dev.grgames.data.models.Developer;
import dev.grgames.data.request.ErrorConnectionException;
import dev.grgames.data.request.response.models.DevelopersResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
public class DevelopersRequest {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    private final WebClient webClient;
    @Value("${rawg.host}") private String host;
    @Value("${rawg.api-key}") private String key;
    @Value("${developer.endpoint}") private String endpoint;
    @Value("${maxPageSize.queryParam}") private String pageSize;
    @Value("${developer.maxNumberPage}") private String maxNumberPage;
    public DevelopersRequest(WebClient webClient) {this.webClient = WebClientConfig.webClient();}

    public List<Developer> getData(){
        List<Developer> result = new ArrayList<>();
        DevelopersResponse developers = null;
        ResponseEntity<DevelopersResponse> getDevelopersResponse = null;

        for(Integer i = 1; i <= Integer.parseInt(maxNumberPage); i++){
            try{
                getDevelopersResponse = doRequest(i);
            }catch (ErrorConnectionException e){
                logger.warn("error on developers request", e.getStatus());
            }
            developers = getDevelopersResponse.getBody();
            result.addAll(developers.getResults());
        }
        return result;
    }
    private ResponseEntity<DevelopersResponse> doRequest(Integer page){
        return this.webClient
                .get()
                .uri(this.composeURI(page))
                .retrieve()
                .onStatus(HttpStatusCode::isError, response ->{
                    return Mono.error(new ErrorConnectionException(response.statusCode(), ""));
                })
                .toEntity(DevelopersResponse.class)
                .block();
    }


    private URI composeURI(Integer page){
        String uri = host + endpoint + "?page=" + page + "&page_size=" + pageSize + "&key=" + key;
        UriBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
        return uriBuilder.build();
    }
}
