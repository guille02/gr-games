package dev.grgames.data.request.models;

import dev.grgames.GrGamesApplication;
import dev.grgames.config.web.WebClientConfig;
import dev.grgames.data.models.*;
import dev.grgames.data.request.ErrorConnectionException;
import dev.grgames.data.request.response.models.GamesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class GamesRequest {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    private final WebClient webClient;
    @Value("${wiremock.host}") private String host;
    @Value("${game.endpoint}") private String endpoint;
    @Value("${pageNumber.queryParam}") private String queryParam;
    public GamesRequest(WebClient webClient) {this.webClient = WebClientConfig.webClient();}
    @Autowired GameRequest gameRequest;

    public List<Game> getData(){
        List<Game> result = new ArrayList<>();
        GamesResponse games = null;
        ResponseEntity<GamesResponse> getGamesResponse = null;

        for(int i = 1; i <= 1; i++){
            try{
                getGamesResponse = doRequest(1);
            }catch (ErrorConnectionException e){
                logger.warn("error on games request", e.getStatus());
            }
        }

        games = getGamesResponse.getBody();
        result.addAll(Objects.requireNonNull(completeGamesInfo(games)));
        setStores(result);
        setPlatforms(result);
        return result;
    }

    private ResponseEntity<GamesResponse> doRequest(Integer page){
        return this.webClient
                .get()
                .uri(this.composeURI(page))
                .retrieve()
                .onStatus(HttpStatusCode::isError, response ->{
                    return Mono.error(new ErrorConnectionException(response.statusCode(), ""));
                })
                .toEntity(GamesResponse.class)
                .block();
    }

    private URI composeURI(Integer page){
        String uri = host+endpoint+"?"+queryParam+"="+page;
        UriBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
        return uriBuilder.build();
    }

    private void setStores(List<Game> games) {
        games.stream().forEach(game -> {
            List<Store> stores = game.getStoreElems()
                    .stream()
                    .map(StoreElem::getStore)
                    .collect(Collectors.toList());
            game.setStores(stores);
        });
    }

    private void setPlatforms(List<Game> games) {
        games.stream().forEach(game -> {
            List<Platform> platforms = game.getPlatformElems()
                    .stream()
                    .map(PlatformElem::getPlatform)
                    .collect(Collectors.toList());
            game.setPlatforms(platforms);
        });
    }

    private List<Game> completeGamesInfo(GamesResponse games){
        return games.getResults()
                .stream()
                .map(game -> gameRequest.getData(game.getSlug()))
                .collect(Collectors.toList());
    }
}

