package dev.grgames.data.request.models;

import dev.grgames.GrGamesApplication;
import dev.grgames.config.web.WebClientConfig;
import dev.grgames.data.models.Store;
import dev.grgames.data.request.ErrorConnectionException;
import dev.grgames.data.request.response.models.StoresResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Service
public class StoresRequest {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    private final WebClient webClient;
    @Value("${wiremock.host}") private String host;
    @Value("${store.endpoint}") private String endpoint;
    public StoresRequest(WebClient webClient) {this.webClient = WebClientConfig.webClient();}
    public List<Store> getData(){
        List<Store> result = new ArrayList<>();
        StoresResponse stores = null;
        ResponseEntity<StoresResponse> getStoresResponse = null;

        try{
            getStoresResponse = doRequest();
        }catch (ErrorConnectionException e){
            logger.warn("error on stores request", e.getStatus());
        }
        stores = getStoresResponse.getBody();
        result.addAll(Objects.requireNonNull(stores).getResults());

        return result;
    }

    private ResponseEntity<StoresResponse> doRequest(){
        return this.webClient
                .get()
                .uri(this.composeURI())
                .retrieve()
                .onStatus(HttpStatusCode::isError, response ->{
                    return Mono.error(new ErrorConnectionException(response.statusCode(), ""));
                })
                .toEntity(StoresResponse.class)
                .block();
    }

    private URI composeURI(){
        String uri = host+endpoint;
        UriBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
        return uriBuilder.build();
    }
}
