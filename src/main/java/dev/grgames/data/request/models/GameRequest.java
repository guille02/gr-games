package dev.grgames.data.request.models;

import dev.grgames.GrGamesApplication;
import dev.grgames.config.web.WebClientConfig;
import dev.grgames.data.models.Game;
import dev.grgames.data.request.ErrorConnectionException;
import dev.grgames.data.request.response.models.GameResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;

@Service
public class GameRequest {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    private final WebClient webClient;
    @Value("${rawg.host}") private String host;
    @Value("${game.endpoint}") private String endpoint;
    @Value("${rawg.api-key}") private String queryParam;
    public GameRequest(WebClient webClient) {this.webClient = WebClientConfig.webClient();}

    public Game getData(String slug){
        Game result = null;
        GameResponse game = null;
        ResponseEntity<Game> getGameResponse = null;

        try{
            getGameResponse = doRequest(slug);
        }catch (ErrorConnectionException e){
            logger.warn("error on game request", e.getStatus());
        }
        result = getGameResponse.getBody();
        return result;
    }

    private ResponseEntity<Game> doRequest(String slug){
        return this.webClient
                .get()
                .uri(this.composeURI(slug))
                .retrieve()
                .onStatus(HttpStatusCode::isError, response ->{
                    return Mono.error(new ErrorConnectionException(response.statusCode(), ""));
                })
                .toEntity(Game.class)
                .block();
    }

    private URI composeURI(String slug){
        String uri = host+endpoint+"/"+slug+"?key="+queryParam;
        UriBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
        return uriBuilder.build();
    }
}
