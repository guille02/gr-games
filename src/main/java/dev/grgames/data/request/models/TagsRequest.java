package dev.grgames.data.request.models;

import dev.grgames.GrGamesApplication;
import dev.grgames.config.web.WebClientConfig;
import dev.grgames.data.models.Tag;
import dev.grgames.data.request.ErrorConnectionException;
import dev.grgames.data.request.response.models.TagsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class TagsRequest {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    private final WebClient webClient;
    @Value("${rawg.host}") private String host;
    @Value("${rawg.api-key}") private String key;
    @Value("${tag.endpoint}") private String endpoint;
    @Value("${maxPageSize.queryParam}") private String pageSize;
    @Value("${tags.maxNumberPage}") private String maxNumberPage;
    public TagsRequest(WebClient webClient) {this.webClient = WebClientConfig.webClient();}

    public List<Tag> getData(){
        List<Tag> result = new ArrayList<>();
        TagsResponse tags = null;
        ResponseEntity<TagsResponse> getTagsResponse = null;

        for(Integer i = 1; i <= Integer.parseInt(maxNumberPage); i++){
            try{
                getTagsResponse = doRequest(i);
            }catch (ErrorConnectionException e){
                logger.warn("error on tags request", e.getStatus());
            }
            tags = getTagsResponse.getBody();
            result.addAll(Objects.requireNonNull(tags).getResults());
        }
        return result;
    }


    private ResponseEntity<TagsResponse> doRequest(Integer page){
        return this.webClient
                .get()
                .uri(this.composeURI(page))
                .retrieve()
                .onStatus(HttpStatusCode::isError, response ->{
                    return Mono.error(new ErrorConnectionException(response.statusCode(), ""));
                })
                .toEntity(TagsResponse.class)
                .block();
    }

    private URI composeURI(Integer page){
        String uri = host + endpoint + "?page=" + page + "&page_size=" + pageSize + "&key=" + key;
        UriBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
        return uriBuilder.build();
    }
}
