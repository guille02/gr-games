package dev.grgames.data.request.models;

import dev.grgames.GrGamesApplication;
import dev.grgames.config.web.WebClientConfig;
import dev.grgames.data.models.Screenshot;
import dev.grgames.data.request.ErrorConnectionException;
import dev.grgames.data.request.response.models.ScreenshotsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ScreenshotsRequest {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    private final WebClient webClient;
    @Value("${rawg.host}") private String host;
    @Value("${game.endpoint}") private String gamesEndpoint;
    @Value("${screenshot.endpoint}") private String screenshotsEndpoint;
    @Value("${rawg.api-key}") private String queryParam;
    public ScreenshotsRequest(WebClient webClient) {this.webClient = WebClientConfig.webClient();}

    public List<Screenshot> getData(String slug){
        List<Screenshot> result = new ArrayList<>();
        ScreenshotsResponse screenshots = null;
        ResponseEntity<ScreenshotsResponse> getScreenshotsResponse = null;

        try{
            getScreenshotsResponse = doRequest(slug);
        }catch (ErrorConnectionException e){
            logger.warn("error on screenshots request", e.getStatus());
        }
        screenshots = getScreenshotsResponse.getBody();
        result.addAll(Objects.requireNonNull(screenshots).getResults());

        return result;
    }

    private ResponseEntity<ScreenshotsResponse> doRequest(String slug){
        return this.webClient
                .get()
                .uri(this.composeURI(slug))
                .retrieve()
                .onStatus(HttpStatusCode::isError, response ->{
                    return Mono.error(new ErrorConnectionException(response.statusCode(), ""));
                })
                .toEntity(ScreenshotsResponse.class)
                .block();
    }
    private URI composeURI(String slug){
        String uri = host + gamesEndpoint + "/" + slug + screenshotsEndpoint + "?key=" + queryParam;
        UriBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
        return uriBuilder.build();
    }
}
