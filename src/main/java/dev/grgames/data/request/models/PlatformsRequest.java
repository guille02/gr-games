package dev.grgames.data.request.models;

import dev.grgames.GrGamesApplication;
import dev.grgames.config.web.WebClientConfig;
import dev.grgames.data.models.Platform;
import dev.grgames.data.request.ErrorConnectionException;
import dev.grgames.data.request.response.models.PlatformsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PlatformsRequest {
    private static final Logger logger = LoggerFactory.getLogger(GrGamesApplication.class);
    private final WebClient webClient;
    @Value("${wiremock.host}") private String host;
    @Value("${platform.endpoint}") private String endpoint;
    public PlatformsRequest(WebClient webClient) {this.webClient = WebClientConfig.webClient();}

    public List<Platform> getData(){
        List<Platform> result = new ArrayList<>();
        PlatformsResponse platforms = null;
        ResponseEntity<PlatformsResponse> platformsResponse = null;

        try{
            platformsResponse = doRequest();
        }catch (ErrorConnectionException e){
            logger.warn("error on platforms request", e.getStatus());
        }
        platforms = platformsResponse.getBody();
        result.addAll(Objects.requireNonNull(platforms).getResults());

        return result;
    }
    private ResponseEntity<PlatformsResponse> doRequest(){
        return this.webClient
                .get()
                .uri(this.composeURI())
                .retrieve()
                .onStatus(HttpStatusCode::isError, response ->{
                    return Mono.error(new ErrorConnectionException(response.statusCode(), ""));
                })
                .toEntity(PlatformsResponse.class)
                .block();
    }
    private URI composeURI(){
        String uri = host+endpoint;
        UriBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(uri);
        return uriBuilder.build();
    }
}
