package dev.grgames.data.request.response.models;

import dev.grgames.data.models.Developer;

import java.util.List;

public class DevelopersResponse {
    private List<Developer> results;

    public List<Developer> getResults() {
        return results;
    }
}
