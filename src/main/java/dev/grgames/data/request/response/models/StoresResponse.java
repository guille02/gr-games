package dev.grgames.data.request.response.models;

import dev.grgames.data.models.Store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StoresResponse implements Serializable {
    List<Store> results = new ArrayList<>();
    private Integer count;

    public List<Store> getResults() {
        return results;
    }

    public Integer getCount() {
        return count;
    }
}
