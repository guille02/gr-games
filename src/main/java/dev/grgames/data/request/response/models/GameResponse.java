package dev.grgames.data.request.response.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import dev.grgames.data.models.*;

import java.util.Date;
import java.util.List;

public class GameResponse {
    @JsonProperty
    private String slug;
    @JsonProperty
    private String name;
    @JsonProperty
    private String description;
    @JsonProperty
    private Date released;
    @JsonProperty
    private String background_image;
    @JsonProperty
    private String background_image_additional;
    @JsonProperty
    private int playtime;
    @JsonProperty
    private String website;
    @JsonProperty
    private List<PlatformElem> platformElems;
    @JsonProperty
    private List<StoreElem> storeElems;
    @JsonProperty
    private List<Developer> developers;
    @JsonProperty
    private List<Tag> tags;
    @JsonProperty
    private List<Genre> genres;
}
