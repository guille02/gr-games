package dev.grgames.data.request.response.models;

import dev.grgames.data.models.Screenshot;

import java.util.ArrayList;
import java.util.List;

public class ScreenshotsResponse {
    List<Screenshot> results = new ArrayList<>();
    private Integer count;
    public List<Screenshot> getResults() {
        return results;
    }
    public Integer getCount() {
        return count;
    }
}
