package dev.grgames.data.request.response.models;

import dev.grgames.data.models.Platform;

import java.util.ArrayList;
import java.util.List;

public class PlatformsResponse {
    List<Platform> results = new ArrayList<>();
    private Integer count;
    public List<Platform> getResults() {
        return results;
    }
    public Integer getCount() {
        return count;
    }
}
