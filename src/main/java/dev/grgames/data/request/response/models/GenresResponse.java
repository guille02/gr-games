package dev.grgames.data.request.response.models;

import dev.grgames.data.models.Genre;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GenresResponse implements Serializable {
    private List<Genre> results = new ArrayList<>();
    private Integer count;
    private String next;
    private String previous;
    public List<Genre> getResults() {
        return results;
    }
    public Integer getCount() {
        return count;
    }

    public String getNext() {
        return next;
    }

    public String getPrevious() {
        return previous;
    }
}
