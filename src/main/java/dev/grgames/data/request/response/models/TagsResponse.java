package dev.grgames.data.request.response.models;

import dev.grgames.data.models.Tag;

import java.util.ArrayList;
import java.util.List;

public class TagsResponse {
    List<Tag> results = new ArrayList<>();
    private Integer count;

    public List<Tag> getResults() {
        return results;
    }
    public Integer getCount() {
        return count;
    }

}
