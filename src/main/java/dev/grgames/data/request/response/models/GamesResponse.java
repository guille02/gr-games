package dev.grgames.data.request.response.models;

import dev.grgames.data.models.Game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GamesResponse implements Serializable {
    private List<Game> results = new ArrayList<>();
    private Integer count;
    private String next;
    private String previous;
    public List<Game> getResults() {
        return results;
    }
}