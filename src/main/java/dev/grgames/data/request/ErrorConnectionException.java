package dev.grgames.data.request;

import org.springframework.http.HttpStatusCode;

/**
 * Excepcion propia para controlar los errores producidos en llamadas API.
 */
public class ErrorConnectionException extends RuntimeException {
    private int code;
    private String message;
    private HttpStatusCode status;

    public ErrorConnectionException(int code, String message1) {
        this.code = code;
        this.message = message1;
    }
    public ErrorConnectionException(HttpStatusCode status, String message) {
        this.status = status;
        this.code = status.value();
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public HttpStatusCode getStatus() {
        return status;
    }
}
