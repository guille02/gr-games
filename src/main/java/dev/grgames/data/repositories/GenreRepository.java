package dev.grgames.data.repositories;

import dev.grgames.data.models.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre,String>, JpaSpecificationExecutor<Genre> {
    Genre findBySlug(String slug);
}
