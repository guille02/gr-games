package dev.grgames.data.repositories;

import dev.grgames.data.models.Game;
import dev.grgames.data.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, String>, JpaSpecificationExecutor<Game> {
    Tag findBySlug(String slug);
}
