package dev.grgames.data.repositories;

import dev.grgames.data.models.Platform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformRepository extends JpaRepository<Platform,String>, JpaSpecificationExecutor<Platform> {
    Platform findBySlug(String slug);
}
