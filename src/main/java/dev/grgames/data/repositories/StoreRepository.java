package dev.grgames.data.repositories;


import dev.grgames.data.models.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store,String>, JpaSpecificationExecutor<Store> {
        Store findBySlug(String slug);
}
