package dev.grgames.data.repositories;


import dev.grgames.data.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;



@Repository
public interface GameRepository extends JpaRepository<Game,String>, JpaSpecificationExecutor<Game> {
     Game findBySlug(String slug);
}
