package dev.grgames.data.repositories;

import dev.grgames.data.models.Screenshot;
import org.springframework.data.repository.CrudRepository;

public interface ScreenshotRepository extends CrudRepository<Screenshot, Integer> {
}
