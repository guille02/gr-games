package dev.grgames.data.repositories;

import dev.grgames.data.models.Developer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DeveloperRepository extends JpaRepository<Developer, String>, JpaSpecificationExecutor<Developer> {
    Developer findBySlug(String slug);
}
