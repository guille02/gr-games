package dev.grgames.service;

import dev.grgames.data.models.messaging.NewUserMessage;
import dev.grgames.utils.JsonUtils;
import lombok.RequiredArgsConstructor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SubscriberService {
    private static final Logger logger = LoggerFactory.getLogger(SubscriberService.class);

    private final JmsTemplate topicJmsTemplate;

    private final UserService userService;

    public void publishToTopic(String topic, String message){
        topicJmsTemplate.convertAndSend(topic, message);
    }

    @JmsListener( destination = "sign-up", containerFactory = "jmsFactoryTopic")
    public void listenTopicSingUp(String message) {
        logger.info("Sign Up user message received");
        Optional<NewUserMessage> newUserMessage = JsonUtils.fromJson(message);
        if(newUserMessage.isEmpty()) {
            logger.warn("Error saving user: role not found");
            return;
        }
        userService.saveUser(newUserMessage.get().getUsername(), newUserMessage.get().getRole());
    }

    @JmsListener( destination = "tema2", containerFactory = "jmsFactoryTopic")
    public void listenTopic2(String message) {
        logger.info("Mensaje del tema 2 publicado:: " + message);
    }

    @JmsListener( destination = "tema3", containerFactory = "jmsFactoryTopic")
    public void listenTopic3(String message) {
        logger.info("Mensaje del tema 3 publicado:: " + message);
    }

    @JmsListener( destination = "tema4", containerFactory = "jmsFactoryTopic")
    public void listenTopic4(String message) {
        logger.info("Mensaje del tema 4 publicado:: " + message);
    }

    @JmsListener( destination = "${forum.userId}")
    public void listenDirectMessage(String message) {
        logger.info("Se ha recibido un mensaje privado: " + message);
    }
}
