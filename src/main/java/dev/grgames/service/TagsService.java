package dev.grgames.service;

import dev.grgames.controllers.model.TagRequestResponse;
import dev.grgames.controllers.model.TagsResponse;
import dev.grgames.data.models.Tag;
import dev.grgames.data.repositories.TagRepository;
import dev.grgames.utils.Patcher;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TagsService {

    private final TagRepository tagRepository;

    private final Patcher patcher;

    public Optional<TagRequestResponse> getTag(String slug) {
        var tag = tagRepository.findBySlug(slug);
        if(tag == null) return Optional.empty();
        return Optional.of(getTagResponse(tag));
    }

    public Optional<TagsResponse> getTags(PageRequest pageRequest) {
        return Optional.of(getTagsRespose(tagRepository.findAll(pageRequest).stream().toList()));
    }

    public Optional<TagRequestResponse> modifyEntireTag(TagRequestResponse tag) {
        Tag updatedTag = getTagFromTagResponse(tag);
        Tag existTag = tagRepository.findBySlug(tag.getSlug());
        if(existTag == null) return Optional.empty();
        tagRepository.save(updatedTag);
        return Optional.of(tag);
    }

    public Optional<TagRequestResponse> modifyPartialTag(String slug, TagRequestResponse incompleteTag) {
        Tag completeTag = tagRepository.findBySlug(slug);
        if(completeTag == null) return Optional.empty();
        try{
            patcher.internPatcher(completeTag,getTagFromTagResponse(incompleteTag));
            tagRepository.save(completeTag);
            return Optional.of(getTagResponse(completeTag));
        }catch (IllegalAccessException e){
            return Optional.empty();
        }
    }

    private Tag getTagFromTagResponse(TagRequestResponse tagRequestResponse) {
        return new Tag(
                tagRequestResponse.getSlug(),
                tagRequestResponse.getName(),
                tagRequestResponse.getLanguage(),
                tagRequestResponse.getGames_count(),
                tagRequestResponse.getImage_background()
        );
    }

    private TagsResponse getTagsRespose(List<Tag> tags){
        return new TagsResponse(
                tags.stream().map(this::getTagResponse).toList()
        );
    }

    private TagRequestResponse getTagResponse(Tag tag) {
        return new TagRequestResponse(
                tag.getSlug(),
                tag.getName(),
                tag.getLanguage(),
                tag.getGames_count(),
                tag.getImage_background()
        );
    }
}
