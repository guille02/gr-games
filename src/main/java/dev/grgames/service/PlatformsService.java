package dev.grgames.service;

import dev.grgames.controllers.model.PlatformRequestResponse;
import dev.grgames.controllers.model.PlatformsResponse;
import dev.grgames.data.models.Platform;
import dev.grgames.data.repositories.PlatformRepository;
import dev.grgames.utils.Patcher;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PlatformsService {

    private final PlatformRepository platformRepository;

    private final Patcher patcher;

    public Optional<PlatformRequestResponse> getPlatform(String slug) {
        var platform = platformRepository.findBySlug(slug);
        if(platform == null) return Optional.empty();
        return Optional.of(getPlatformResponse(platform));
    }

    public Optional<PlatformsResponse> getPlatforms(PageRequest pageRequest) {
        return Optional.of(getPlatformsResponse(platformRepository
                .findAll(pageRequest)
                .stream()
                .toList()));
    }

    public Optional<PlatformRequestResponse> modifyEntirePlatform(PlatformRequestResponse platformRequestResponse) {
        Platform updatedPlatform = getPlatformFromPlatformResponse(platformRequestResponse);
        Platform existPlatform = platformRepository.findBySlug(platformRequestResponse.getSlug());
        if(existPlatform == null) return Optional.empty();
        platformRepository.save(updatedPlatform);
        return Optional.of(platformRequestResponse);
    }

    public Optional<PlatformRequestResponse> modifyPartialPlatform(String slug, PlatformRequestResponse incompletePlatform) {
        Platform completePlatform = platformRepository.findBySlug(slug);
        if(completePlatform == null) return Optional.empty();
        try{
            patcher.internPatcher(completePlatform, getPlatformFromPlatformResponse(incompletePlatform));
            platformRepository.save(completePlatform);
            return Optional.of(getPlatformResponse(completePlatform));
        }catch (IllegalAccessException e) {
            return Optional.empty();
        }
    }

    private Platform getPlatformFromPlatformResponse(PlatformRequestResponse platformRequestResponse) {
        return new Platform(
                platformRequestResponse.getSlug(),
                platformRequestResponse.getName(),
                platformRequestResponse.getImage_background(),
                platformRequestResponse.getGames_count()

        );
    }

    private PlatformsResponse getPlatformsResponse(List<Platform> platforms){
        return new PlatformsResponse(
                platforms.stream()
                        .map(this::getPlatformResponse)
                        .toList()
        );
    }

    private PlatformRequestResponse getPlatformResponse(Platform platform){
        return new PlatformRequestResponse(
                platform.getSlug(),
                platform.getName(),
                platform.getGames_count(),
                platform.getImage_background()
        );
    }


}
