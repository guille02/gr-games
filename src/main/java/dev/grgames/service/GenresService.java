package dev.grgames.service;

import dev.grgames.controllers.model.GenreRequestResponse;
import dev.grgames.controllers.model.GenresResponse;
import dev.grgames.data.models.Genre;
import dev.grgames.data.repositories.GenreRepository;
import dev.grgames.utils.Patcher;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GenresService {
    @Autowired
    private GenreRepository genreRepository;

    private final Patcher patcher;

    public Optional<GenreRequestResponse> getGenre(String slug) {
        var genre = genreRepository.findBySlug(slug);
        if(genre == null) return Optional.empty();
        return Optional.of(getGenreRequestResponseFromGenre(genre));
    }

    public Optional<GenresResponse> getGenres(PageRequest pageRequest) {
        return Optional.of(getGenresResponse(genreRepository
                .findAll(pageRequest)
                .stream()
                .toList()));
    }

    public Optional<GenreRequestResponse> modifyEntireGenre(GenreRequestResponse genre){
        Genre updatedGenre = getGenreFromGenreRequestResponse(genre);
        Genre existGenre = genreRepository.findBySlug(genre.getSlug());
        if(existGenre == null) return Optional.empty();
        genreRepository.save(updatedGenre);
        return Optional.of(genre);
    }

    public Optional<GenreRequestResponse> modifyPartialGenre(String slug, GenreRequestResponse incompleteGame) {
        Genre completeGenre = genreRepository.findBySlug(slug);
        if(completeGenre == null) return Optional.empty();
        try{
            patcher.internPatcher(completeGenre,getGenreFromGenreRequestResponse(incompleteGame));
            genreRepository.save(completeGenre);
            return Optional.of(getGenreRequestResponseFromGenre(completeGenre));
        }catch (IllegalAccessException e){
            return Optional.empty();
        }
    }

    private GenresResponse getGenresResponse(List<Genre> genres){
        return new GenresResponse(
                genres.stream()
                        .map(this::getGenreRequestResponseFromGenre)
                        .toList()
        );
    }

    private GenreRequestResponse getGenreRequestResponseFromGenre(Genre genre){
        return new GenreRequestResponse(
                genre.getSlug(),
                genre.getName(),
                genre.getGames_count(),
                genre.getImage_background(),
                genre.getDescription()
        );
    }

    private Genre getGenreFromGenreRequestResponse(GenreRequestResponse genreRequestResponse){
        return new Genre(
                genreRequestResponse.getSlug(),
                genreRequestResponse.getName(),
                genreRequestResponse.getGames_count(),
                genreRequestResponse.getImage_background(),
                genreRequestResponse.getDescription()
        );
    }


}
