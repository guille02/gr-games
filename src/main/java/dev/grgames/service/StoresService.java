package dev.grgames.service;

import dev.grgames.controllers.model.StoreRequestResponse;
import dev.grgames.controllers.model.StoresResponse;
import dev.grgames.data.models.Store;
import dev.grgames.data.repositories.StoreRepository;
import dev.grgames.utils.Patcher;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StoresService {

    private final StoreRepository storeRepository;

    private final Patcher patcher;

    public Optional<StoreRequestResponse> getStore(String slug) {
        var store = storeRepository.findBySlug(slug);
        if(store == null) return Optional.empty();
        return Optional.of(getStoreResponse(store));
    }

    public Optional<StoresResponse> getStores(PageRequest pageRequest) {
        return Optional.of(getStoresResponse(storeRepository.findAll(pageRequest).stream().toList()));
    }

    public Optional<StoreRequestResponse> modifyEntireStore(StoreRequestResponse storeRequestResponse) {
        Store updatedStore = getStoreFromStoreResponse(storeRequestResponse);
        Store existStore = storeRepository.findBySlug(storeRequestResponse.getSlug());
        if(existStore == null) return Optional.empty();
        storeRepository.save(updatedStore);
        return Optional.of(storeRequestResponse);
    }

    public Optional<StoreRequestResponse> modifyPartialStore(String slug, StoreRequestResponse incompleteStore) {
        Store completeStore = storeRepository.findBySlug(slug);
        if(completeStore == null) return Optional.empty();
        try {
            patcher.internPatcher(completeStore,getStoreFromStoreResponse(incompleteStore));
            storeRepository.save(completeStore);
            return Optional.of(getStoreResponse(completeStore));
        }catch (IllegalAccessException e) {
            return Optional.empty();
        }
    }

    private Store getStoreFromStoreResponse(StoreRequestResponse storeRequestResponse) {
        return new Store(
                storeRequestResponse.getSlug(),
                storeRequestResponse.getName(),
                storeRequestResponse.getWebSite(),
                storeRequestResponse.getImage_background(),
                storeRequestResponse.getGames_count()
        );
    }

    private StoresResponse getStoresResponse(List<Store> stores) {
        return new StoresResponse(
                stores.stream()
                        .map(this::getStoreResponse)
                        .toList()
        );
    }

    private StoreRequestResponse getStoreResponse(Store store) {
        return new StoreRequestResponse(
                store.getSlug(),
                store.getName(),
                store.getWebsite(),
                store.getImage_background(),
                store.getGames_count()
        );
    }
}
