package dev.grgames.service;

import dev.grgames.data.models.Role;
import dev.grgames.data.models.User;
import dev.grgames.data.repositories.RoleRepository;
import dev.grgames.data.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    public boolean isUser(String username){
        return userRepository.findByUserName(username) != null;
    }

    public User saveUser(String username, String roleName){
        Role role = roleRepository.findByName(roleName);
        if(role == null) logger.info("Error saving user: role not found");
        return userRepository.save(new User(username, role));
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUserName(username);
    }
}
