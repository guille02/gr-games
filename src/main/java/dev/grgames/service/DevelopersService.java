package dev.grgames.service;

import dev.grgames.controllers.model.DeveloperRequestResponse;
import dev.grgames.controllers.model.DevelopersResponse;
import dev.grgames.data.models.Developer;
import dev.grgames.data.repositories.DeveloperRepository;
import dev.grgames.utils.Patcher;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DevelopersService {

    private final DeveloperRepository developerRepository;

    private final Patcher patcher;

    public Optional<DeveloperRequestResponse> getDeveloper(String slug) {
        var developer = developerRepository.findBySlug(slug);
        if(developer == null) return Optional.empty();
        return Optional.of(getDeveloperResponse(developer));
    }

    public Optional<DevelopersResponse> getDevelopers(PageRequest pageRequest) {
        return Optional.of(getDevelopersResponse(developerRepository.findAll(pageRequest).stream().toList()));
    }

    public Optional<DeveloperRequestResponse> modifyEntireDeveloper(DeveloperRequestResponse developerRequestResponse) {
        Developer updatedDeveloper = getDeveloperFromDeveloperResponse(developerRequestResponse);
        Developer existDeveloper = developerRepository.findBySlug(developerRequestResponse.getSlug());
        if(existDeveloper == null) return Optional.empty();
        developerRepository.save(updatedDeveloper);
        return Optional.of(developerRequestResponse);
    }

    public Optional<DeveloperRequestResponse> modifyPartialDeveloper(String slug, DeveloperRequestResponse partialDeveloper) {
        Developer completeDeveloper = developerRepository.findBySlug(slug);
        if (completeDeveloper == null) return Optional.empty();
        try {
            patcher.internPatcher(completeDeveloper, getDeveloperFromDeveloperResponse(partialDeveloper));
            developerRepository.save(completeDeveloper);
            return Optional.of(getDeveloperResponse(completeDeveloper));
        } catch (IllegalAccessException e) {
            return Optional.empty();
        }
    }

    private Developer getDeveloperFromDeveloperResponse(DeveloperRequestResponse developerRequestResponse) {
        return new Developer(
                developerRequestResponse.getSlug(),
                developerRequestResponse.getName(),
                developerRequestResponse.getGames_count(),
                developerRequestResponse.getImage_background()
        );
    }

    private DevelopersResponse getDevelopersResponse(List<Developer> developers) {
        return new DevelopersResponse(
                developers.stream().map(this::getDeveloperResponse).toList()
        );
    }

    private DeveloperRequestResponse getDeveloperResponse(Developer developer) {
        return new DeveloperRequestResponse(
                developer.getSlug(),
                developer.getName(),
                developer.getGames_count(),
                developer.getImage_background()
        );
    }


}
