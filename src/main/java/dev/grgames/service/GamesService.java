package dev.grgames.service;

import dev.grgames.controllers.model.GameRequestResponse;
import dev.grgames.controllers.model.GamesResponse;
import dev.grgames.data.models.*;
import dev.grgames.data.repositories.GameRepository;
import dev.grgames.utils.Patcher;
import dev.grgames.utils.SearchCriteria;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class GamesService {

    private final GameRepository gameRepository;

    private final UserService userService;

    private final Patcher patcher;

    public Optional<GameRequestResponse> getGame(String slug) {
        var game = gameRepository.findBySlug(slug);
        if(game == null) return Optional.empty();
        return Optional.of(getGameRequestResponseFromGame(game));
    }

    public Optional<GamesResponse> getGames(PageRequest pageRequest, String name, String genres, String platforms, String stores, String tags, String developers, String username) {
        List<Genre> genresList = null;
        List<Platform> platformsList = null;
        List<Store> storesList = null;
        List<Tag> tagsList = null;
        List<Developer> developerList = null;

        if(username != null && !userService.isUser(username)) return Optional.empty();

        if(StringUtils.hasLength(name) || StringUtils.hasLength(genres) || StringUtils.hasLength(platforms) || StringUtils.hasLength(stores) || StringUtils.hasLength(tags) || StringUtils.hasLength(developers) || StringUtils.hasLength(username)){
            if(StringUtils.hasLength(genres))  genresList = Stream.of(genres.split(",")).map(Genre::new).toList();
            if(StringUtils.hasLength(platforms)) platformsList = Stream.of(platforms.split(",")).map(Platform::new).toList();
            if(StringUtils.hasLength(stores)) storesList = Stream.of(stores.split(",")).map(Store::new).toList();
            if(StringUtils.hasLength(tags)) tagsList = Stream.of(tags.split(",")).map(Tag::new).toList();
            if(StringUtils.hasLength(developers)) developerList = Stream.of(developers.split(",")).map(Developer::new).toList();
            return Optional.of(getGamesResponse(gameRepository
                    .findAll(new SearchCriteria<>(genresList,platformsList,storesList,tagsList,developerList,username),pageRequest).stream().toList()));
        }
        return Optional.of(getGamesResponse(gameRepository.findAll(pageRequest).stream().toList()));
    }

    public Optional<GameRequestResponse> modifyEntireGame(GameRequestResponse game) {
        Game updatedGame = getGameFromGameRequestResponse(game);
        Game existGame = gameRepository.findBySlug(game.getSlug());
        if(existGame == null) return Optional.empty();
        gameRepository.save(updatedGame);
        return Optional.of(game);
    }

    public Optional<GameRequestResponse> modifyPartialGame(String slug, GameRequestResponse partialGame)  {
        Game completeGame = gameRepository.findBySlug(slug);
        if(completeGame == null) return Optional.empty();
        try {
            patcher.internPatcher(completeGame,getGameFromGameRequestResponse(partialGame));
            gameRepository.save(completeGame);
            return Optional.of(getGameRequestResponseFromGame(completeGame));
        } catch (IllegalAccessException e) {
            return Optional.empty();
        }
    }

    public void deleteGame(String slug) {
        gameRepository.deleteById(slug);
    }


    private GamesResponse getGamesResponse(List<Game> games){
        return new GamesResponse(
                games.stream().map(this::getGameRequestResponseFromGame).toList()
        );
    }

    private GameRequestResponse getGameRequestResponseFromGame(Game game){
        return new GameRequestResponse(
                game.getSlug(),
                game.getName(),
                game.getDescription(),
                game.getReleased(),
                game.getBackground_image(),
                game.getBackground_image_additional(),
                game.getPlaytime(),
                game.getWebsite(),
                game.getRatingAverage(),
                game.getGenres(),
                game.getStores(),
                game.getPlatforms(),
                game.getTags(),
                game.getDevelopers()
        );
    }

    private Game getGameFromGameRequestResponse(GameRequestResponse game){
        return new Game(
                game.getSlug(),
                game.getName(),
                game.getDescription(),
                game.getReleased(),
                game.getBackground_image(),
                game.getBackground_image_additional(),
                game.getPlaytime(),
                game.getWebsite(),
                game.getRatingAverage(),
                game.getGenres(),
                game.getStores(),
                game.getPlatforms(),
                game.getTags(),
                game.getDevelopers()
        );
    }
}
